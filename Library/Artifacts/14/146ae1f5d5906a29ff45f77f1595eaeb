                                           0.0.0 ţ˙˙˙      ˙˙f!ë59Ý4QÁóB   í          7  ˙˙˙˙                 Ś ˛                       E                    Ţ  #                     . ,                     5   a                    Ţ  #                     . ,                      r                    Ţ  #      	               . ,      
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    ń  J   ˙˙˙˙   Ŕ           1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               \     ˙˙˙˙               H r   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     H w   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     H    ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                     Ţ  #      !               . ,      "                   ˙˙˙˙#   @          1  1  ˙˙˙˙$               Ţ      %               . j     &               Ő    ˙˙˙˙'               1  1  ˙˙˙˙(    Ŕ            Ţ      )                  j  ˙˙˙˙*                H   ˙˙˙˙+               1  1  ˙˙˙˙,   @            Ţ      -                Q  j     .                y 
    /                 Ţ  #      0               . ,      1                 §      2    @            ž ś      3    @            Ţ  #      4               . ,      5               H ť   ˙˙˙˙6              1  1  ˙˙˙˙7   @            Ţ      8                Q  j     9                H Ć   ˙˙˙˙:              1  1  ˙˙˙˙;   @            Ţ      <                Q  j     =                H Ř   ˙˙˙˙>              1  1  ˙˙˙˙?   @            Ţ      @                Q  j     A              MonoImporter PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_ExternalObjects SourceAssetIdentifier type assembly name m_UsedFileIDs m_DefaultReferences executionOrder icon m_UserData m_AssetBundleName m_AssetBundleVariant     s    ˙˙ŁGń×ÜZ56 :!@iÁJ*          7  ˙˙˙˙                 Ś ˛                        E                    Ţ                       .                      (   a                    Ţ                       .                       r                    Ţ        	               .       
               H Ť ˙˙˙˙             1  1  ˙˙˙˙   @           Ţ                     Q  j                    H ę ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     ń  =   ˙˙˙˙              1  1  ˙˙˙˙               Ţ                       j  ˙˙˙˙               H   ˙˙˙˙              1  1  ˙˙˙˙   @            Ţ                      Q  j                     y 
                    Ţ                       .                      y Q                       Ţ                       .                       Ţ  X      !                H i   ˙˙˙˙"              1  1  ˙˙˙˙#   @            Ţ      $                Q  j     %                H u   ˙˙˙˙&              1  1  ˙˙˙˙'   @            Ţ      (                Q  j     )              PPtr<EditorExtension> m_FileID m_PathID PPtr<PrefabInstance> m_DefaultReferences m_Icon m_ExecutionOrder m_ClassName m_Namespace                        \       ŕyŻ     `       ź             %X ˘Q E˘#ŠtžÄ                                                                                                                 ŕyŻ                š*                                                                AnalyticSDFCollider ,  using System;
using UnityEngine;
using UnityEngine.Rendering;
using System.Runtime.InteropServices;
using com.zibra.liquid.Solver;

namespace com.zibra.liquid.SDFObjects
{
    /// <summary>
    /// An analytical ZibraFluid SDF collider
    /// </summary>
    [AddComponentMenu("Zibra/Zibra Analytic Collider")]
    public class AnalyticSDFCollider : SDFCollider
    {
        /// <summary>
        /// Types of Analytical SDF's
        /// </summary>
        public enum SDFType
        {
            Sphere,
            Box,
            Bowl
        }
        ;

        /// <summary>
        /// Currently chosen type of SDF collider
        /// </summary>
        public SDFType chosenSDFType = SDFType.Sphere;

        /// <summary>
        /// Initialize the analytical SDF collider in the given solver instance
        /// </summary>
        /// <param name="InstanceID">Istance ID of the solver instance</param>
        public void Initialize(int InstanceID)
        {
            if (IsInitialized.Contains(InstanceID))
                return;

            ColliderIndex = AllColliders.IndexOf(this);
            ZibraLiquidBridge.RegisterAnalyticCollider(InstanceID, ColliderIndex);
            IsInitialized.Add(InstanceID);
        }

        public void Start()
        {
            colliderParams = new ColliderParams();
            NativeDataPtr = Marshal.AllocHGlobal(Marshal.SizeOf(colliderParams));
        }

        private void ComputeSDF_Base(int InstanceID, CommandBuffer SolverCommandBuffer, ComputeBuffer Coordinates,
                                     ComputeBuffer Output, ComputeBuffer OutputID, int ID, int Elements,
                                     CalculationType type)
        {
            Initialize(InstanceID);

            Vector3 position = transform.position;
            Quaternion rotation = transform.rotation;
            Vector3 scale = transform.lossyScale;
            Vector3 newpos = position;

            colliderParams.Position = newpos;
            colliderParams.Rotation = new Vector4(rotation.x, rotation.y, rotation.z, rotation.w);
            colliderParams.Scale = scale;
            colliderParams.Elements = Elements;
            colliderParams.OpType = (int)type;
            colliderParams.CurrentID = ID;
            colliderParams.SDFType = (int)chosenSDFType;
            colliderParams.colliderIndex = ColliderIndex;

            Marshal.StructureToPtr(colliderParams, NativeDataPtr, true);
            SolverCommandBuffer.IssuePluginEventAndData(
                ZibraLiquidBridge.RunSDFShaderWithDataPtr(),
                ZibraLiquidBridge.EventAndInstanceID(ZibraLiquidBridge.EventID.ComputeAnalyticSDF, InstanceID),
                NativeDataPtr);
        }

        /// <summary>
        /// Compute the SDF and unite it with the SDF in Output
        /// </summary>
        /// <param name="InstanceID">Istance ID of the solver instance</param>
        /// <param name="SolverCommandBuffer">Command buffer to add the SDF computation to</param>
        /// <param name="Coordinates">Buffer with input cooditates whre the SDF needs to be sampled</param>
        /// <param name="Output">Output SDF</param>
        /// <param name="OutputID">Output ID of the object at each point</param>
        /// <param name="ID">Istance ID of the SDF collider</param>
        /// <param name="Elements">Number of points to compute the SDF for</param>
        public override void ComputeSDF_Unite(int InstanceID, CommandBuffer SolverCommandBuffer,
                                              ComputeBuffer Coordinates, ComputeBuffer Output, ComputeBuffer OutputID,
                                              int ID, int Elements)
        {
            ComputeSDF_Base(InstanceID, SolverCommandBuffer, Coordinates, Output, OutputID, ID, Elements,
                            CalculationType.Unite);
        }
    }
}       š*            AnalyticSDFCollider    com.zibra.liquid.SDFObjects 