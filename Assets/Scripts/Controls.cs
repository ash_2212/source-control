using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            transform.position = transform.position + new Vector3(-5, 0, 0) * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.position = transform.position + new Vector3(5, 0, 0) * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.Z))
        {
            transform.position = transform.position + new Vector3(0, 0, 5) * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.S))
        {
            transform.position = transform.position + new Vector3(0, 0, -5) * Time.deltaTime;
        }
    }
}
