using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public GameObject PlayMenu;
    public GameObject Settings;
    void Start()
    {
        BackButton();
    }

    public void PlayButton()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
    }
    
    public void SettingsButton()
    {
        PlayMenu.SetActive(false);
        Settings.SetActive(true);
    }

    public void BackButton()
    {
        PlayMenu.SetActive(true);
        Settings.SetActive(false);
    }

    public void QuitButton()
    {
        Application.Quit();
    }

}
