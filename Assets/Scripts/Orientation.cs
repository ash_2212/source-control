using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orientation : MonoBehaviour
{
    private void Update()
    {
        Vector2 mousePosition = new Vector2(
  Input.GetAxis("MouseX"),
  Input.GetAxis("MouseY")
);
        
        transform.LookAt(mousePosition);
    }
}